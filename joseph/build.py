import os
from dataclasses import dataclass
import datetime

from pydriller import Repository
import markdown
import jinja2
from bs4 import BeautifulSoup
from feedgen.feed import FeedGenerator

template_loader = jinja2.FileSystemLoader(searchpath="./templates")
template_env = jinja2.Environment(loader=template_loader)

index_template = template_env.get_template("index.html")


@dataclass
class Post:
    slug: str
    abs_path: str
    published_date: datetime.datetime
    _markdown_content = None
    _html_content = None
    _soup = None

    @property
    def markdown_content(self):
        if self._markdown_content is None:
            with open(self.abs_path) as f:
                self._markdown_content = f.read()

        return self._markdown_content

    @property
    def html_content(self):
        return self.soup.prettify()

    @property
    def soup(self):
        if self._soup is None:
            self._html_content = markdown.markdown(self.markdown_content)
            self._soup = BeautifulSoup(self._html_content, 'html.parser')

            for heading_level in range(4, 0, -1):
                # Bump all headers down one level
                for heading in self._soup.find_all(f'h{heading_level}'):
                    heading.name = f'h{heading_level + 1}'
        return self._soup

    @property
    def title(self):
        return self.soup.find('h2').string


def build_feed(posts):
    fg = FeedGenerator()
    fg.title('Schilz.org')
    fg.subtitle('Husband, software engineer.')
    fg.link( href='https://www.schilz.org', rel='self' )
    fg.language('en')

    for post in posts[::-1]:
        fe = fg.add_entry()
        fe.id(post.slug)
        fe.title(post.title)
        fe.link(href="https://www.schilz.org")

    return fg.rss_str(pretty=True)


def get_published_date(project_relative_path):
    for commit in Repository("..", filepath=project_relative_path, order="reverse").traverse_commits():
        if commit.msg.lower().startswith('publish') and project_relative_path in [f.new_path for f in commit.modified_files]:
            return commit.author_date

    return None


def gather_articles():
    results = []
    for year in list(map(str, range(2022, 2100))):
        if os.path.exists(year):
            for path in os.listdir(year):
                if path.endswith(".md"):
                    project_relative_path = os.path.join(
                        "joseph",
                        year,
                        path
                    )
                    abs_path = os.path.abspath(
                        os.path.join(
                            year,
                            path,
                        )
                    )

                    published_date = get_published_date(project_relative_path)

                    if published_date:
                        results.append(
                            Post(
                                slug=os.path.basename(path).replace('.md', ''),
                                abs_path=abs_path,
                                published_date=published_date
                            )
                        )

    return results

posts = sorted(gather_articles(), key=lambda post: post.published_date.timestamp(), reverse=True)

with open("../public/~joseph/feed.rss", "w") as f:
    f.write(build_feed(posts).decode())

with open("../public/~joseph/index.html", "w") as f:
    f.write(index_template.render(posts=posts))




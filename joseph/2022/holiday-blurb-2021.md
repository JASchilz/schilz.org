# Holiday Blurb - 2021

Day 638 of our captivity. After more than a year of being forced to cohabitate by circumstances beyond our control, Becca and Joe were married on July 7th 2021. Thank you to our parents who accepted, supported, and witnessed this event. Our reception will follow in 2022.

We also welcomed a new life into our home this year: Odo Bennett-Schilz, adopted in October. And although Odo looks like an overgrown cross between a Chihuahua and a Beagle, our little blessing is 50% Poodle and 15% Cocker Spaniel!

This year, Becca revisited three titles from the Call of Duty franchise and her second playthrough of Spiderman: Miles Morales. We’re also about halfway through Star Trek: Deep Space 9, and we rounded out our Fast and Furious epic epidemic experience with this year’s F9. The second biggest day this year for Joe was receiving 4 pallets of insulation from Lowes, and he’s still installing it to this day.

Our goals for this year are: to bring our kitchen into the 21st century, more walks, more books, and a camping trip or two.

<figure>
    <img src="static/odo-stairs-2021.png"
      loading="lazy" alt="Odo on the stairs" width="600">
    <figcaption>
        Odo posing on the stairs.
    </figcaption>
</figure>



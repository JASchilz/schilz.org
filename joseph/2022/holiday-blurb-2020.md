# Holiday Blurb - 2020

Day 273 of our captivity. After initial months of conflict, we have grown comfortable, and now glad for the company that fate has provided for us.

This last nine months we have: watched the entire Fast and Furious, Harry Potter, and Lord of the Rings series and are working our way through Battlestar Galactica; Becca is on her second playthrough of  GTA V for the year and Joe is working through the original Star Trek series has reached the final Honor Harrington military sci-fi book.

Thank you for your love and support this year! Our goals for next year are humble: more walks, more books, and a camping trip or two.

